class Rabbit{
    constructor(type){
        this.type = type;        
    }
    speak(line){
        console.log(`${this.type} hello ${line}`);
    }
}

let killerRabbit = new Rabbit("killer"); 
killerRabbit.speak("i'm alive");