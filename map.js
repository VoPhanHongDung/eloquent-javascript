// function map(array,transform){
//     let mapped = [];
//     for(let element of array){
//         mapped.push(transform(element))
//     }
//     return mapped;
// }

// let array = [1,2,3];
// function transform(number){
//     return number * 10;
// }

// console.log(map(array,transform));


let ages = {
    joey : 39,
    ross : 20,
}


console.log(`age of joey`, "joey" in ages);
console.log(`age's joey ${ages.joey}`);
console.log(`age's joey ${ages["joey"]}`);

let ageMap = new Map();
ageMap.set("Joey",20);
console.log(ageMap.get("Joey"));
console.log("Is Jack's age known?", ages.has("Jack")); // → Is Jack's age known? false